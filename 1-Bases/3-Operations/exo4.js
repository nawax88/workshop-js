/**
 * Tester les différentes opérations avec les types primitifs (et non primitifs) du chapitre précédent.
 * Afin de vous faire une idée de ce qu'accepte, gère ou foire complètement, Javascript.
 * Cet exercice est à tester via la commande `node 1-Bases/3-Operations/exo4.js`
 */
