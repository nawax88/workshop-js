/**
 * Créer une variable tableau avec pour valeurs : 1,2,3 et 4
 * Utiliser une boucle For pour ajouter 3 à tous les chiffres du tableau
 */

// Ne changez pas cette partie du code !!
test('Transformation', () => {
  expect(tableau).toEqual([4, 5, 6, 7]);
});
