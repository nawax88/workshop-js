/**
 * Utilisez la boucle while pour multiplier la variable `resultat` par 2, 5 fois.
 * La valeur initiale de la variable est : 1
 */

// Codez ...

// Ne changez pas cette partie du code !!
test('While', () => {
  expect(resultat).toEqual(2 * 2 * 2 * 2 * 2);
});
