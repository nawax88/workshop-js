# workshop-js

## Pré-requis

Vous devez avoir installer NodeJs v12 minimum.
Pour vérifier, ouvrez un terminal et exécutez :
```
node -v
```
La version affichér en X.Y.Z doit afficher un X égal à 12 ou plus.

## Installation
```
npm install
```

### Utilisation
Le cours est organisé dans un ordre numérique, le chiffre devant les noms de dossiers permettant de saovir dans quel ordre les réaliser.
A l'intérieur de ces dossier les thèmes abordés seronts eux aussi organisés de manière numérique et sont donc à réaliser dans l'ordre.

Un exercice contient l'énoncé de son problème. Il vous faut remplacer la partie `// Codez...` par votre code. Sauf précision contraire, vous pouvez vérifier si vous avez réussi votre exercice en exécutant la commande suivante :
```
npm run test -- X-cours/Y-theme/exo.test.js
```
Où X-cours et Y-theme sont les noms des dossiers dans lesquels vous voulez faire votre exercice.

Ne faites pas attention au bloc de code
```
test(...)
```
En dessous de la où vous codez, cette partie est la pour tester que votre code soit bon.